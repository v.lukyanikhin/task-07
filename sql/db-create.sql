DROP database IF EXISTS test2db;

CREATE database test2db;

USE test2db;

CREATE TABLE IF NOT EXISTS `test2db`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login` (`login` ASC) VISIBLE);

CREATE TABLE IF NOT EXISTS `test2db`.`teams` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(10) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
  );

CREATE TABLE IF NOT EXISTS `test2db`.`users_teams` (
  `user_id` INT NULL DEFAULT NULL,
  `team_id` INT NULL DEFAULT NULL,
  UNIQUE INDEX `user_id` (`user_id` ASC, `team_id` ASC) VISIBLE,
  INDEX `team_idx` (`team_id` ASC) VISIBLE,
  CONSTRAINT `user`
    FOREIGN KEY (`user_id`)
    REFERENCES `test2db`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `team`
    FOREIGN KEY (`team_id`)
    REFERENCES `test2db`.`teams` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);



SELECT * FROM users;
SELECT * FROM teams;
SELECT * FROM users_teams;
