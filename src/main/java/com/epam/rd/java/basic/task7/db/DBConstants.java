package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    protected DBConstants() {
        //nothing to do
    }

    // USER QUERIES
    public static final String INSERT_USER = "INSERT INTO users(id,login) values (DEFAULT,?)";
    public static final String FIND_ALL_USERS_ORDER_BY_ID = "SELECT*FROM users ORDER BY id";
    public static final String FIND_CONCRETE_USER_BY_LOGIN = "SELECT*FROM users WHERE login=?";
    public static final String FIND_CONCRETE_USER_BY_ID = "SELECT*FROM users WHERE id=?";
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM users WHERE login=?";


    // TEAM QUERIES
    public static final String INSERT_TEAM = "INSERT INTO teams(id,name) values (DEFAULT,?)";
    public static final String FIND_ALL_TEAMS = "SELECT*FROM teams";
    public static final String FIND_CONCRETE_TEAM_BY_NAME = "SELECT*FROM teams WHERE name=?";
    public static final String FIND_CONCRETE_TEAM_BY_ID = "SELECT*FROM teams WHERE id=?";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE name LIKE ?";
    public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";

    //USERS_TEAMS QUERIES
    public static final String FIND_USERS_TEAMS = "SELECT * FROM teams WHERE id in(SELECT team_id FROM users_teams WHERE user_id=?)";
    public static final String SET_USERS_TEAMS = "INSERT INTO users_teams (user_id,team_id) values (?,?)";



}
