package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    private static String FULL_URL;

    private static Connection connection;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {
        Properties properties = new Properties();

        try (FileReader reader = new FileReader("app.properties")) {
            properties.load(reader);
            FULL_URL = properties.getProperty("connection.url");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private User getUser(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }

    public List<User> findAllUsers() {
        List<User> users = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DBConstants.FIND_ALL_USERS_ORDER_BY_ID);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                users.add(getUser(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DBConstants.INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
            int k = 0;
            stmt.setString(++k, user.getLogin());
            stmt.executeUpdate();
            ResultSet ids = stmt.getGeneratedKeys();
            if (ids.next()) {
                user.setId(ids.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("User was not added", e);
        }
        return true;
    }

    private void transactionRollback(Connection con) {
        try {
            con.rollback();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    private void close(AutoCloseable stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteUsers(User... users) {
        Connection con = null;
        PreparedStatement stmt = null;
        int deleted = 0;
        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(DBConstants.DELETE_USER_BY_LOGIN);
            for (User user : users) {
                stmt.setString(1, user.getLogin());
                stmt.executeUpdate();
                deleted = stmt.getUpdateCount();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return deleted > 0;
        } finally {
            close(stmt);
            close(con);
        }
        return deleted > 0;
    }

    public User getUser(String login) {
        Connection con = null;
        PreparedStatement stmt = null;
        User user = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(DBConstants.FIND_CONCRETE_USER_BY_LOGIN);
            stmt.setString(1, login);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    user = getUser(rs);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(stmt);
            close(con);
        }

        return user;
    }

    private Team getTeam(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }

    public Team getTeam(String name) {
        Connection con = null;
        PreparedStatement stmt = null;
        Team team = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(DBConstants.FIND_CONCRETE_TEAM_BY_NAME);
            stmt.setString(1, name);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    team = getTeam(rs);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(stmt);
            close(con);
        }

        return team;
    }

    public List<Team> findAllTeams() {
        List<Team> teams = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DBConstants.FIND_ALL_TEAMS);
             ResultSet rs = stmt.executeQuery()) {
            while (rs.next()) {
                teams.add(getTeam(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return teams;
    }

    public boolean insertTeam(Team team) {
        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DBConstants.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
            int k = 0;
            stmt.setString(++k, team.getName());
            stmt.executeUpdate();
            ResultSet ids = stmt.getGeneratedKeys();
            if (ids.next()) {
                team.setId(ids.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            con.setAutoCommit(false);

            for (Team team : teams) {
                stmt = con.prepareStatement(DBConstants.SET_USERS_TEAMS);
                stmt.setInt(1, getUser(user.getLogin()).getId());
                stmt.setInt(2, getTeam(team.getName()).getId());
                stmt.executeUpdate();
            }

            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            transactionRollback(con);
            throw new DBException("Something went wrong, teams were not added", e.getCause());
        } finally {
            close(stmt);
            close(con);
        }
        return true;
    }


    public List<Team> getUserTeams(User user) {
        List<Team> usersTeams = new ArrayList<>();
        User userFromDB = getUser(user.getLogin());

        try (Connection con = DriverManager.getConnection(FULL_URL);
             PreparedStatement stmt = con.prepareStatement(DBConstants.FIND_USERS_TEAMS)) {
            stmt.setString(1, String.valueOf(userFromDB.getId()));
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    usersTeams.add(getTeam(rs));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usersTeams;
    }

    public boolean deleteTeam(Team team) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(DBConstants.DELETE_TEAM);
            stmt.setString(1, team.getName());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(stmt);
            close(con);
        }
        return true;
    }

    public boolean updateTeam(Team team) {
        Connection con = null;
        PreparedStatement stmt = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
            stmt = con.prepareStatement(DBConstants.UPDATE_TEAM);
            int k = 0;
            stmt.setString(++k, team.getName());
            stmt.setInt(++k, team.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(stmt);
            close(con);
        }
        return true;
    }

}
